package org.lyrcl.lib.pirate;

import java.nio.charset.Charset;

import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

/**
 * ZeroMQ 2.2 socket convenience class.
 */
public class PSocket {
    /**
     * The charset to use for string (un)subscriptions.
     */
    private static final Charset UTF8 = Charset.forName("UTF8");

    /**
     * The underlying ZMQ socket.
     */
    private final ZMQ.Socket socket;

    /**
     * The ZMQ context of the underlying socket.
     */
    private final ZMQ.Context context;

    /**
     * The timeout to receive a message.
     */
    private int recvTimeout = -1;

    /**
     * The timeout to send a message.
     */
    private int sendTimeout = -1;

    /**
     * Initialise a new PSocket instance using the specified context with
     * the specified type.
     *
     * @param context The ZMQ context of this socket.
     * @param type The type of this socket.
     */
    public PSocket(ZMQ.Context context, int type) {
        this.context = context;
        this.socket = context.socket(type);
    }

    /**
     * ZeroMQ 2.2 poller convenience class.
     */
    public static class Poller {
        /**
         * The underlying ZMQ poller.
         */
        private final ZMQ.Poller poller;

        /**
         * Initialise a Poller instance that wraps the specified ZMQ.Poller.
         *
         * @param poller The ZMQ.Poller to wrap.
         */
        private Poller(ZMQ.Poller poller) {
            this.poller = poller;
        }

        /**
         * Also listen for incoming messages on the specified PSocket.
         *
         * @param socket The additional socket to listen on.
         * @return This (for chaining).
         */
        public Poller with(PSocket socket) {
            this.poller.register(socket.socket, ZMQ.Poller.POLLIN);
            return this;
        }

        /**
         * Get the underlying ZMQ Poller.
         *
         * @return The underlying ZMQ Poller.
         */
        public ZMQ.Poller poller() {
            return this.poller;
        }

        /**
         * Poll for incoming messages with no timeout.
         *
         * @return The number of sockets with new messages, or -1 if an error
         *         occurred.
         */
        public long poll() {
            return this.poll(-1);
        }

        /**
         * Poll for incoming messages using the specified timeout.
         *
         * @param timeout The maximum time to wait, in milliseconds.
         * @return The number of sockets with new messages, or -1 if an error
         *         occurred.
         */
        public long poll(int timeout) {
            long microTimeout = timeout == -1 ? -1 : ((long)timeout * 1000);
            return this.poller.poll(microTimeout);
        }
    }

    /**
     * Send a one-part message containing the specified bytes.
     *
     * @param bytes The message contents.
     * @return true if send was successful, false otherwise.
     */
    public Boolean send(byte[] bytes) {
        return this.send(bytes, -1);
    }

    /**
     * Send a one-part message containing the specified bytes with the
     * specified timeout.
     *
     * @param bytes The message contents.
     * @param timeout Send timeout, in milliseconds.
     * @return true if send was successful, false otherwise.
     */
    public Boolean send(byte[] bytes, int timeout) {
        return this.send(bytes, timeout, true);
    }

    /**
     * Send a message containing the specified bytes with the specified
     * timeout.
     *
     * @param bytes The message contents.
     * @param timeout Send timeout, in milliseconds.
     * @param last true if this is a single-part message or the last frame of
     *             a multi-part message, false otherwise.
     * @return true if send was successful, false otherwise.
     */
    public Boolean send(byte[] bytes, int timeout, boolean last) {
        int flags = last ? 0 : ZMQ.SNDMORE;
        this.setSendTimeoutIfNeeded(timeout);
        return this.socket.send(bytes, flags);
    }

    /**
     * Set the send timeout of the underlying socket if it is different to the
     * current timeout.
     *
     * @param timeout The send timeout, in milliseconds.
     */
    private void setSendTimeoutIfNeeded(int timeout) {
        if (this.sendTimeout != timeout) {
            this.socket.setSendTimeOut(timeout);
            this.sendTimeout = timeout;
        }
    }

    /**
     * Receive a single-part message.
     *
     * @return The contents of a single-part message, or null if interrupted.
     */
    public byte[] recv() {
        return this.recv(-1);
    }

    /**
     * Receive a single-part message with the specified timeout.
     *
     * @param timeout The receive timeout, in milliseconds.
     * @return The contents of a single-part message, or null if interrupted
     *         or timed out.
     */
    public byte[] recv(int timeout) {
        this.setRecvTimeoutIfNeeded(timeout);
        return this.socket.recv(0);
    }

    /**
     * Receive a multi-part message.
     *
     * @return The multi-part message, or null if interrupted.
     */
    public ZMsg recvMulti() {
        return this.recvMulti(-1);
    }

    /**
     * Receive a multi-part message with the specified timeout.
     *
     * @param timeout The receive timeout, in milliseconds.
     * @return The multi-part message, or null if interrupted or timed out.
     */
    public ZMsg recvMulti(int timeout) {
        this.setRecvTimeoutIfNeeded(timeout);
        return ZMsg.recvMsg(this.socket);
    }

    /**
     * Set the receive timeout to the specified number of milliseconds.
     *
     * @param timeout The receive timeout in milliseconds.
     */
    private void setRecvTimeoutIfNeeded(int timeout) {
        if (this.recvTimeout != timeout) {
            this.socket.setReceiveTimeOut(timeout);
            this.recvTimeout = timeout;
        }
    }

    /**
     * Bind to the specified endpoint.
     *
     * @param endpoint The endpoint to bind to.
     * @return This (for chaining).
     */
    public PSocket bind(String endpoint) {
        this.socket.bind(endpoint);
        return this;
    }

    /**
     * Connect to the specified endpoint.
     *
     * @param endpoint The endpoint to bind to.
     * @return This (for chaining).
     */
    public PSocket connect(String endpoint) {
        this.socket.connect(endpoint);
        return this;
    }

    /**
     * Subscribe to messages starting with the specified string prefix, encoded
     * as ASCII/UTF-8.
     *
     * @param prefix The string prefix.
     * @return This (for chaining).
     */
    public PSocket sub(String prefix) {
        return this.sub(prefix.getBytes(PSocket.UTF8));
    }

    /**
     * Subscribe to all messages.
     *
     * @return This (for chaining).
     */
    public PSocket sub() {
        return this.sub(new byte[0]);
    }

    /**
     * Subscribe to messages starting with the specified bytes.
     *
     * @param prefix The byte prefix.
     * @return This (for chaining).
     */
    public PSocket sub(byte[] prefix) {
        this.socket.subscribe(prefix);
        return this;
    }

    /**
     * Remove a subscription to messages starting with the specified string
     * prefix, encoded as ASCII/UTF-8.
     *
     * @param prefix The string prefix.
     * @return This (for chaining).
     */
    public PSocket unsub(String prefix) {
        return this.unsub(prefix.getBytes(PSocket.UTF8));
    }

    /**
     * Remove a subscription to all messages.
     *
     * @return This (for chaining).
     */
    public PSocket unsub() {
        return this.unsub(new byte[0]);
    }

    /**
     * Remove a subscription to messages starting with the specified bytes.
     *
     * @param prefix The byte prefix.
     * @return This (for chaining).
     */
    public PSocket unsub(byte[] prefix) {
        this.socket.unsubscribe(prefix);
        return this;
    }

    /**
     * Drop all outgoing messages and connects and close this socket.
     */
    public void dropAndClose() {
        this.socket.setLinger(0);
        this.socket.close();
    }

    /**
     * Close this socket, waiting for all outgoing messages and connects to
     * complete.
     */
    public void close() {
        this.socket.close();
    }

    /**
     * Check for incoming messages detected by a previous poll.
     *
     * @return true if messages are waiting to be received, false otherwise.
     */
    public Boolean pollIn() {
        return ((int)this.socket.getEvents() & ZMQ.Poller.POLLIN) > 0;
    }

    /**
     * Create a poller that polls for incoming messages on this instance.
     *
     * @return A poller that listens for incoming messages on this instance.
     */
    public PSocket.Poller poller() {
        return new Poller(this.context.poller()).with(this);
    }

    /**
     * Create a poller that polls for incoming messages on this instance and
     * the specified socket.
     *
     * @param socket The other socket to listen on.
     * @return A poller that polls for incoming messages on this instance and
     *         the specified socket.
     */
    public PSocket.Poller with(PSocket socket) {
        return this.poller().with(socket);
    }

    /**
     * Get the underlying ZMQ.Socket.
     *
     * @return The underlying ZMQ.Socket.
     */
    public ZMQ.Socket socket() {
        return socket;
    }
}
