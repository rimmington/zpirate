package org.lyrcl.lib.pirate;

/**
 * Pirate protocol constants.
 */
public final class PirateProtocol {
    /**
     * The bytes signifying a new worker is ready for work.
     */
    public static byte[] READY = new byte[] { 1 };

    /**
     * The bytes signifying an existing component is still alive.
     */
    public static byte[] HEARTBEAT = new byte[] { 2 };
}
