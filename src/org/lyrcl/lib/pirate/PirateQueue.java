package org.lyrcl.lib.pirate;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.logging.Logger;

import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

/**
 * ZeroMQ 2.2 paranoid pirate queue implementation.
 */
public class PirateQueue implements Runnable {
    /**
     * The logger for this class.
     */
    private static final Logger LOGGER =
            Logger.getLogger(PirateQueue.class.getName());

    /**
     * The number of heartbeats to wait before declaring a worker dead.
     */
    private final int retries;

    /**
     * The time between heartbeats to and from workers, in nanoseconds.
     */
    private final int heartbeatInterval;

    /**
     * The endpoint to communicate with clients on.
     */
    private final String clientEndpoint;

    /**
     * The endpoint to communicate with workers on.
     */
    private final String workerEndpoint;

    /**
     * This queue's ZMQ context.
     */
    private final ZMQ.Context context;

    /**
     * A remote worker with an address and expiry.
     */
    private class Worker {
        /**
         * The address of the worker (used in message wrapping).
         */
        public byte[] address;

        /**
         * The time when the worker will be removed from the queue due to
         * inactivity, in nanoseconds.
         *
         * @see System.nanoTime()
         */
        public long expiry;

        /**
         * Initialise a Worker instance with the specified address and
         * an automatically calculated expiry.
         *
         * @param address The address of the worker.
         */
        public Worker(byte[] address) {
            this.address = address;
            this.expiry = System.currentTimeMillis() +
                    PirateQueue.this.retries * PirateQueue.this.heartbeatInterval;
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(this.address);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof Worker &&
                Arrays.equals(this.address,
                    ((Worker) other).address);
        }
    }

    /**
     * A queue of live workers available for work.
     */
    private class WorkerQueue {
        /**
         * The available workers.
         */
        private final Deque<Worker> queue = new ArrayDeque<Worker>();

        /**
         * The next time to send out heartbeats, in nanoseconds.
         *
         * @see System.nanoTime()
         */
        private long heartbeatAt = System.nanoTime()
                + PirateQueue.this.heartbeatInterval;

        /**
         * Add a worker available for work to the end of the queue.
         *
         * @param worker The worker.
         */
        public void ready(Worker worker) {
            this.queue.remove(worker);
            this.queue.addLast(worker);
        }

        /**
         * Pop a worker from the head of the queue and return its address.
         *
         * @return The address of a worker available for work.
         */
        public byte[] next() {
            return this.queue.pop().address;
        }

        /**
         * Check if the queue is empty.
         *
         * @return true if no workers are available for work, false otherwise.
         */
        public boolean isEmpty() {
            return this.queue.isEmpty();
        }

        /**
         * Send out heartbeats to available workers and remove expired workers.
         *
         * @param workerSock The socket to send heartbeats to workers from.
         */
        public void maintain(PSocket workerSock) {
            if (System.nanoTime() >= this.heartbeatAt) {
                for (Worker w: this.queue) {
                    workerSock.send(w.address, -1, false);
                    workerSock.send(PirateProtocol.HEARTBEAT, -1, true);
                }

                this.heartbeatAt = System.nanoTime() +
                    PirateQueue.this.heartbeatInterval;
            }

            long time = System.nanoTime();
            Iterator<Worker> i = this.queue.iterator();
            while (i.hasNext() && i.next().expiry < time) {
                i.remove();
            }
        }
    }

    /**
     * Initialise a new paranoid pirate queue instance attached to the specified
     * endpoints with the default liveness and performance settings.
     *
     * @param clientEndpoint The endpoint to communicate with clients on.
     * @param workerEndpoint The endpoint to communicate with workers on.
     */
    public PirateQueue(String clientEndpoint, String workerEndpoint) {
        this(clientEndpoint, workerEndpoint, 1000, 3, 1);
    }

    /**
     * Initialise a new paranoid pirate queue instance attached to the specified
     * endpoints with the specified liveness and performance settings.
     *
     * @param clientEndpoint The endpoint to communicate with clients on.
     * @param workerEndpoint The endpoint to communicate with workers on.
     * @param heartbeatInterval The time between heartbeats to and from workers,
     *                          in milliseconds.
     * @param retries The number of heartbeats to wait before declaring a
     *                worker dead.
     * @param ioThreads The number of ZeroMQ I/O threads to use. Allocate
     *                  around 1 thread for each gigabyte/second of data.
     */
    public PirateQueue(String clientEndpoint, String workerEndpoint,
            int heartbeatInterval, int retries, int ioThreads) {
        this.heartbeatInterval = heartbeatInterval * 1000000;
        this.retries = retries;

        this.context = ZMQ.context(ioThreads);
        this.clientEndpoint = clientEndpoint;
        this.workerEndpoint = workerEndpoint;
    }

    /**
     * Run as a paranoid pirate queue until interrupted.
     */
    @Override
    public void run() {
        PSocket clientSock = new PSocket(this.context, ZMQ.ROUTER);
        PSocket workerSock = new PSocket(this.context, ZMQ.ROUTER);
        clientSock.bind(this.clientEndpoint);
        workerSock.bind(this.workerEndpoint);

        PSocket.Poller pollWorkers = workerSock.poller();
        PSocket.Poller pollBoth = workerSock.with(clientSock);
        WorkerQueue workers = new WorkerQueue();

        while (!Thread.currentThread().isInterrupted()) {
            // No point polling for clients if there's no workers
            (workers.isEmpty() ? pollWorkers : pollBoth).poll();

            if (workerSock.pollIn()) {
                ZMsg msg = workerSock.recvMulti();
                if (msg == null) {
                    break; // interrupted
                }

                ZFrame address = msg.unwrap();
                workers.ready(new Worker(address.getData()));

                if (msg.size() == 1) { // control message
                    ZFrame frame = msg.getFirst();
                    if (!Arrays.equals(PirateProtocol.HEARTBEAT, frame.getData()) &&
                            !Arrays.equals(PirateProtocol.READY, frame.getData())) {
                        LOGGER.warning(
                                "Invalid control message received from worker: "
                                        + msg.toString());
                    }
                } else {
                    msg.send(clientSock.socket());
                }
            }

            if (clientSock.pollIn()) {
                ZMsg msg = clientSock.recvMulti();
                if (msg == null) {
                    break; // interrupted
                } else {
                    msg.push(workers.next());
                    msg.send(workerSock.socket());
                }
            }

            workers.maintain(workerSock);
        }

        this.context.term();
    }
}
