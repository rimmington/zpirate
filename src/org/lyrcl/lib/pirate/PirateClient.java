package org.lyrcl.lib.pirate;

import org.zeromq.ZMQ;

/**
 * ZeroMQ 2.2 paranoid pirate client API.
 */
public class PirateClient {
    /**
     * The maximum time to wait for a reply to each attempt, in milliseconds.
     */
    private final int timeout;

    /**
     * The number of times to try a request before giving up.
     */
    private final int retries;

    /**
     * The endpoint to communicate with the queue on.
     */
    private final String queueEndpoint;

    /**
     * This client's ZMQ context.
     */
    private final ZMQ.Context context;

    /**
     * The socket used to communicate with the queue.
     */
    private PSocket socket;

    /**
     * Initialise a new paranoid pirate client instance attached to the specified
     * endpoint with the default timeout and performance settings.
     *
     * @param queueEndpoint The endpoint to communicate with the queue on.
     */
    public PirateClient(String queueEndpoint) {
        this(queueEndpoint, 2500, 3, 1);
    }

    /**
     * Initialise a new paranoid pirate client instance attached to the specified
     * endpoint with the specified timeout and performance settings.
     *
     * @param queueEndpoint The endpoint to communicate with the queue on.
     * @param timeout The maximum time to wait for a reply to each attempt,
     *                in milliseconds.
     * @param retries The number of times to try a request before giving up.
     * @param ioThreads The number of ZeroMQ I/O threads to use. Allocate
     *                  around 1 thread for each gigabyte/second of data.
     */
    public PirateClient(String queueEndpoint, int timeout, int retries, int ioThreads) {
        this.timeout = timeout;
        this.retries = retries;
        this.queueEndpoint = queueEndpoint;

        this.context = ZMQ.context(ioThreads);
    }

    /**
     * Make a synchronous request.
     *
     * @param request The request message.
     * @return The reply, or null for a timeout.
     */
    public byte[] req(byte[] request) {
        for (int i = 0; i < this.retries; i++) {
            this.connectIfNeeded();
            this.socket.send(request, 0);
            byte[] response = this.socket.recv(this.timeout);
            if (response != null) {
                return response;
            } else {
                this.socket.dropAndClose();
                this.socket = null;
            }
        }

        return null;
    }

    /**
     * Finish and close communication with the queue.
     */
    public void close() {
        if (this.socket != null) {
            this.socket.close();
        }

        this.context.term();
    }

    /**
     * Connect to the queue if not already.
     */
    private void connectIfNeeded() {
        if (this.socket == null) {
            this.socket = new PSocket(this.context, ZMQ.REQ);
            this.socket.connect(this.queueEndpoint);
        }
    }
}