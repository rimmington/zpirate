package org.lyrcl.lib.pirate;

import java.util.Arrays;
import java.util.logging.Logger;

import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

/**
 * ZeroMQ 2.2 paranoid pirate worker API.
 */
public class PirateWorker {
    /**
     * The logger for this class.
     */
    private static final Logger LOGGER =
            Logger.getLogger(PirateWorker.class.getName());

    /**
     * This worker's ZMQ context.
     */
    private final ZMQ.Context context;

    /**
     * The endpoint to communicate with the queue on.
     */
    private final String queueEndpoint;

    /**
     * The time between heartbeats to and from the queue, in milliseconds.
     */
    private final int heartbeatInterval;

    /**
     * The time to wait for the queue to restart once it stops responding to
     * heartbeats, in milliseconds.
     */
    private final int reconnectInterval;

    /**
     * The number of heartbeats to wait before declaring the queue dead.
     */
    private final int retries;

    /**
     * The socket used to communicate with the queue.
     */
    private PSocket queue;

    /**
     * A frame containing the address of the client the last request
     * was received from.
     */
    private ZFrame replyTo;

    /**
     * The next time to send out a heartbeat, in nanoseconds.
     *
     * @see System.nanoTime()
     */
    private long heartbeatAt;

    /**
     * Initialise a new paranoid pirate worker instance attached to the
     * specified endpoint with the default liveness and performance settings.
     *
     * @param queueEndpoint The endpoint to communicate with the queue on.
     */
    public PirateWorker(String queueEndpoint) {
        this(queueEndpoint, 1000, 3, 5000, 1);
    }

    /**
     * Initialise a new paranoid pirate queue instance attached to the specified
     * endpoints with the specified liveness and performance settings.
     *
     * @param queueEndpoint The endpoint to communicate with the queue on.
     * @param heartbeatInterval The time between heartbeats to and from the
     *                          queue, in milliseconds.
     * @param retries The number of heartbeats to wait before declaring the
     *                queue dead.
     * @param ioThreads The number of ZeroMQ I/O threads to use. Allocate
     *                  around 1 thread for each gigabyte/second of data.
     */
    public PirateWorker(String queueEndpoint, int heartbeatInterval,
            int retries, int reconnectInterval, int ioThreads) {
        this.queueEndpoint = queueEndpoint;
        this.heartbeatInterval = heartbeatInterval;
        this.reconnectInterval = reconnectInterval;
        this.retries = retries;

        this.context = ZMQ.context(ioThreads);
        this.connect();
    }

    /**
     * Respond to the last request with the specified message and wait for a
     * new request.
     *
     * @param reply The reply message. Can only be null for the initial call.
     * @return The next request, or null if interrupted.
     */
    public byte[] recv(byte[] reply) {
        if (this.replyTo != null && reply == null) {
            throw new IllegalArgumentException(
                    "reply cannot be null for non-initial recv call.");
        }

        if (reply != null) {
            ZMsg msg = new ZMsg();
            msg.add(new ZFrame(reply));

            msg.wrap(this.replyTo);
            msg.send(this.queue.socket(), true);
            this.replyTo = null;
        }

        PSocket.Poller poller = this.queue.poller();
        int retriesLeft = this.retries;
        while (!Thread.currentThread().isInterrupted()) {
            if (poller.poll(this.heartbeatInterval) == -1) {
                break; // interrupted
            }

            if (this.queue.pollIn()) {
                ZMsg msg = this.queue.recvMulti();
                if (msg == null) {
                    break; // interrupted
                } else if (msg.size() == 3) {
                    this.replyTo = msg.unwrap();
                    return msg.getFirst().getData();
                } else if (msg.size() == 1) {
                    if (Arrays.equals(msg.getFirst().getData(),
                            PirateProtocol.HEARTBEAT)) {
                        retriesLeft = this.retries;
                    } else {
                        LOGGER.warning("Invalid control message received from queue: "
                                + msg.toString());
                    }
                } else {
                    LOGGER.warning("Message of unknown type received from queue: "
                            + msg.toString());
                }
            } else if (--retriesLeft == 0) {
                try {
                    Thread.sleep(this.reconnectInterval);
                } catch (InterruptedException e) {
                    break; // interrupted
                }
                this.connect();
                retriesLeft = this.retries;
            }

            this.heartbeatIfNeeded();
        }

        return null;
    }

    /**
     * Finish and close communication to the queue.
     */
    public void close() {
        this.queue.close();
        this.context.term();
    }

    /**
     * Connect to the queue and set heartbeatAt.
     */
    private void connect() {
        this.queue = new PSocket(context, ZMQ.DEALER);
        this.queue.connect(queueEndpoint);
        this.queue.send(PirateProtocol.READY);
        this.heartbeatAt = System.nanoTime() +
                ((long)this.heartbeatInterval * 1000000);
    }

    /**
     * Send a heartbeat to the queue if needed.
     */
    private void heartbeatIfNeeded() {
        if (System.nanoTime() >= this.heartbeatAt) {
            this.queue.send(PirateProtocol.HEARTBEAT);
            this.heartbeatAt = System.nanoTime() +
                    ((long)this.heartbeatInterval * 1000000);
        }
    }
}
