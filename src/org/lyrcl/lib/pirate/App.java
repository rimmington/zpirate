package org.lyrcl.lib.pirate;

/**
 * App that displays version information, plus a few examples.
 */
public class App {
    /**
     * Runs a queue that listens for clients on port 5555 and workers on port
     * 5556.
     */
    public static void queue() {
        new PirateQueue("tcp://*:5555", "tcp://*:5556").run();
    }

    /**
     * Runs a client that connects to tcp://localhost:5555 and expects the
     * server to echo messages sent to it.
     */
    public static void client() {
        System.out.println("Connecting to server…");
        PirateClient client = new PirateClient("tcp://localhost:5555");

        Integer sequence = 0;
        while (!Thread.currentThread().isInterrupted()) {
            // We send a request, then we work to get a reply
            sequence++;
            final byte[] request = sequence.toString().getBytes();
            final byte[] reply = client.req(request);
            if (reply == null) {
                break;
            }

            final String replyString = new String(reply).trim();

            int replySequence = -1;
            try {
                replySequence = Integer.parseInt(replyString);
            } catch (Exception e) {
                // Do nothing
            }

            if (replySequence == sequence) {
                System.out.printf("I: server replied OK (%d)\n",
                        replySequence);
            } else {
                System.out.printf("E: malformed reply from server: (%s)\n",
                        replyString);
            }
        }

        client.close();
    }

    /**
     * Worker that connects to a queue at tcp://localhost:5556 and echos
     * back integers and -1 for anything that does not parse to an integer,
     */
    public static void worker() {
        PirateWorker worker = new PirateWorker("tcp://localhost:5556");
        byte[] work = worker.recv(null);
        while (work != null && !Thread.currentThread().isInterrupted()) {
            Integer n;
            try {
                n = Integer.parseInt(new String(work));
            } catch (NumberFormatException e) {
                work = worker.recv("-1".getBytes());
                continue;
            }

            work = worker.recv(n.toString().getBytes());
        }
    }

    /**
     * Print out version information.
     *
     * @param args Command line arguments (ignored).
     * @throws Exception An exception occurred.
     */
    public static void main(String[] args) throws Exception {
        final Package p = App.class.getPackage();
        final String appName = p.getSpecificationTitle();
        final String versionMaven = p.getSpecificationVersion();
        String[] version = new String[] {"", ""};
        if (p.getImplementationVersion() != null) {
            version = p.getImplementationVersion().split(" ", 2);
        }

        final String fmt = "%-7.7s %-15.15s %s%n";

        System.out.printf(fmt, appName, "version:", versionMaven);
        System.out.printf(fmt, appName, "build time:", version[1]);
        System.out.printf(fmt, appName, "build commit:", version[0]);

        System.out.println();
        org.zeromq.App.main(new String[] {});
    }
}
